window.addEventListener("resize", resizeWindow);
async function resizeWindow() {
    actions.loadLayer(Number.parseInt(sessionStorage.getItem('id_Layer')))
}





let LAYER = [];
let teste = undefined;
const contador = 1;

function saveOptions() {
    let opc        = HTML.validaForm();
    let HEIGHT     = LAYER[1].key.findOne('.fillShape').height() * config.escalaY;
    let WIDTH      = LAYER[1].key.findOne('.fillShape').width()  * config.escalaX;
    let IMAGEM     = LAYER[1].key.attrs.img;
    let ID_PRODUTO = LAYER[1].key.attrs.idProduto;
    let LAST_X     = LAYER[1].key.attrs.x;
    let LAST_Y     = LAYER[1].key.attrs.y;

    LAYER[1].tr.detach(LAYER[1].key);
    LAYER[1].layer.draw();

    $.each(opc, (key, val) => {

        if (Object.keys(val)[0].indexOf('X') > -1) {
            WIDTH = Number(val[Object.keys(val)[0]].valueOf());
        }

        if (Object.keys(val)[0].indexOf('Z') > -1) {
            HEIGHT = Number(val[Object.keys(val)[0]].valueOf());
        }


        let valueProps = Number(val[Object.keys(val)[0]]);
        try {
            LAYER[1].key.findOne('.fillShape').attrs._OPCOES[Object.keys(val)[f]] = valueProps

        } catch (erro) {

            //Verificando se componente possui o array _OPCOES
            console.log('A propriedade _OPCOES Existe ? ' + LAYER[1].key.findOne('.fillShape').attrs.hasOwnProperty('_OPCOES'));

            if (!LAYER[1].key.findOne('.fillShape').attrs.hasOwnProperty('_OPCOES')) {
                LAYER[1].key.findOne('.fillShape').attrs._OPCOES = []
            }
            LAYER[1].key.findOne('.fillShape').attrs._OPCOES[Object.keys(val)] = valueProps
        }

    });

    HEIGHT = HEIGHT / config.escalaY
    WIDTH = WIDTH / config.escalaX

    let Intersection = false;

    const RectClienteComparation = {
        x: LAST_X,
        y: LAST_Y,
        width: WIDTH,
        height: HEIGHT

    }

    LAYER[1].layer.children.each(group => {

        if (group.index === LAYER[1].key.index) {
            return;
        }

        if (group.attrs.isTransformer) {
            return;
        }

        if (haveIntersection(group.getClientRect(), RectClienteComparation)) {

            Intersection = true;
        }

    });

    if (!Intersection) {
        let componente = createShape(WIDTH, HEIGHT, 'principal', 1, IMAGEM, ID_PRODUTO, LAYER[1].key.findOne('.fillShape').attrs._OPCOES);
        let _OPCOES = LAYER[1].key.findOne('.fillShape').attrs._OPCOES;

        LAYER[1].layer.children[LAYER[1].key.index].destroy()


        componente.setX(LAST_X);
        componente.setY(LAST_Y);
        componente.setAttr('_OPCOES', _OPCOES);
        //console.log(componente);

        LAYER[1].key = componente;
        LAYER[1].layer.add(componente);
        LAYER[1].layer.draw();
        LAYER[1].key = undefined;

        $('.opcoes').empty();

        LAYER[1].stage.batchDraw();
    } else {
        alert('Não é possivel redimencionar componente');
    }


}

function addLayer() {
    //METEDO OBSOLETO, NÃO ESTA SENDO UTLIZADO, ESTAMOS TRABALHANDO COM APENAS UMA LAYER NA TELA
    let random = Math.round(Math.random() * 19711);
    let parede = 'parede' + random;
    createLayer(parede.toString(), 1500, 900);
}

function Contador(nome_parede) {
    //ESTE METEDO CRIA UM INDEX VALIDO NA CRIACAO DA LAYER ATUALMENTE O INDEX DA LAYER É SEMPRE 1
    //METEDO OBSOLETO, NÃO ESTA SENDO UTLIZADO, ESTAMOS TRABALHANDO COM APENAS UMA LAYER NA TELA
    let contador = 0;

    this.Increment = function () {
        contador++;
        return contador;
    }

    this.getContador = () => {
        const ret = contador;
        return ret;
    }
}

function AddComponente(idLayer, div, img = null, idProduto = null) {
    //console.clear();
    if (!img) {
        img = "https://www.minhamobilia.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/o/mobilia_moderna_cadeira_imperial_ref_1010-0033_2.jpg";
    }
    $(`.btn-adicionar`).prop('disabled', true);

    let mousemovePosition = undefined;
    let newComponente = createShape(100, 100, div.id, idLayer, img, idProduto, false);
    ////console.log(newComponente)

    LAYER[idLayer]['layer'].add(newComponente);
    LAYER[idLayer]['layer'].draw();

    LAYER[idLayer]['stage'].on('mousemove', function () {
        LAYER[idLayer]['tr']._node = null;
        LAYER[idLayer]['layer'].draw();
        ////console.log(LAYER[idLayer]['tr'])
        $(`.btn-adicionar`).prop('disabled', true);

        let mousePos = LAYER[idLayer]['stage'].getPointerPosition();
        let x = mousePos.x;
        let y = mousePos.y
        let ImageComponent = newComponente.findOne('.fillShape');
        ////console.log(ImageComponent)

        try {
            MAX_WIDTH = document.getElementById(`${div.id}`).clientWidth - ImageComponent.width();
            MAX_HEIGHT = document.getElementById(`${div.id}`).clientHeight - ImageComponent.height();
        } catch (erro) {
            ////console.log(erro);
            MAX_WIDTH = $('.konvajs-content')[0].clientWidth - ImageComponent.width();
            MAX_HEIGHT = $('.konvajs-content')[0].clientHeight - ImageComponent.height();
        }


        let pos = { x: x, y: y };

        let x_abs = pos.x
        let y_abs = pos.y;

        if (pos.x < 0) {
            x_abs = 0;
        }

        if (pos.y < 0) {
            y_abs = 0;
        }

        if (pos.y + newComponente.height() >= MAX_HEIGHT) {
            y_abs = MAX_HEIGHT - newComponente.height();
        }

        if ((newComponente.width()) + pos.x >= MAX_WIDTH) {
            x_abs = MAX_WIDTH - newComponente.width();

        }

        x = x_abs;
        y = y_abs;

        mousemovePosition = { x: x, y: y, width: 100, height: 100 };


        newComponente.setX(x);
        newComponente.setY(y);

        LAYER[idLayer]['layer'].draw();
    });


    LAYER[idLayer]['stage'].on('click tap', function (e) {
        //e.evt.preventDefault();

        let verifyIntersection = false;
        let target = e.target.parent;
        if (!target) {
            target = newComponente;
        }
        try {
            if (LAYER[idLayer]['layer'].children.length > 2) {

                LAYER[idLayer]['layer'].children.each(function (group) {

                    if (group === target) {
                        return;
                    }

                    if (group.attrs.isTransformer) {
                        return;
                    }

                    if (haveIntersection(group.getClientRect(), target.getClientRect())) {

                        ////console.log('Intersection');
                        verifyIntersection = true;
                    }
                });
            } else {
                LAYER[idLayer]['stage'].removeEventListener('mousemove');
                ////console.log('break mousemove');
                $(`.btn-adicionar`).removeAttr('disabled');

            }

            if (!verifyIntersection) {
                ////console.log('break mousemove 22');

                LAYER[idLayer]['stage'].removeEventListener('mousemove');
                $(`.btn-adicionar`).removeAttr('disabled');

            }
        } catch (erro) {
            ////console.log(erro);
        }
    });
}

function deleteElement(index, contador) {
    LAYER[contador]['tr'].detach(LAYER[contador]['attachTranformer']);
    LAYER[contador]['layer'].children[index].destroy();
    LAYER[contador]['layer'].draw();
    $('.buttons .btn-remove').remove();
}

function haveIntersection(r1, r2) {
    return !(
        r2.x >= r1.x + r1.width ||
        r2.x + r2.width <= r1.x ||
        r2.y >= r1.y + r1.height ||
        r2.y + r2.height <= r1.y
    );
}






/* Funções de Criação, Edicação e Manipulação do Stage, Layer e Components*/

function createShape(width = 1, height = 1, idDiv, idLayer, img, idProduto, _OPCOES) {
    console.log('width,height ' + width,height)

    var group = new Konva.Group({
        relativeToDiv: idDiv,
        name: 'groupPrincipal',
        x: Math.random() * width,
        y: Math.random() * height,
        img: img,
        idProduto: idProduto,
        draggable: true,
        dragBoundFunc: function (pos) {
            try {
                MAX_WIDTH = document.getElementById(`${idDiv}`).clientWidth;
                MAX_HEIGHT = document.getElementById(`${idDiv}`).clientHeight;
            } catch (erro) {

                ////console.log(this);

                MAX_WIDTH = document.getElementById(`${this.attrs.relativeToDiv}`).clientWidth;
                MAX_HEIGHT = document.getElementById(`${this.attrs.relativeToDiv}`).clientHeight;
            }

            let x_abs = pos.x
            let y_abs = pos.y;

            if (pos.x < 0) {
                x_abs = 0;
            }

            if (pos.y < 0) {
                y_abs = 0;
            }

            let findOne = this.findOne('.fillShape');

            if (pos.y + (this.findOne('.fillShape').getClientRect().height) >= MAX_HEIGHT) {
                y_abs = MAX_HEIGHT - this.findOne('.fillShape').getClientRect().height;
            }

            if ((this.findOne('.fillShape').getClientRect().width) + pos.x >= MAX_WIDTH) {
                x_abs = MAX_WIDTH - (this.findOne('.fillShape').getClientRect().width);

            }

            return {
                x: x_abs,
                y: y_abs
            }
        }
    });

    var shape = new Konva.Image({
        height: height,
        width: width,
        scaleObject: 1,//ScaleLayer,
        fill: '#00000040',
        name: 'fillShape',
        _OPCOES: _OPCOES
    });

    var imageObj1 = new Image();
    imageObj1.onload = ()=>{
        shape.image(imageObj1);
    };
    imageObj1.src = img;


    var label = createInfo(shape.attrs.width, shape.attrs.height, 1);
    group.add(shape);
    group.add(label);

    var boundingBox = shape.getClientRect({ relativeTo: group });

    var box = new Konva.Rect({
        x: boundingBox.x,
        y: boundingBox.y,
        width: boundingBox.width,
        height: boundingBox.height,
    });
    //group.add(box);
    group.posX = group.x();
    group.posY = group.y();

    ////console.log('group')
    ////console.log(group)
    return group;
}

function createInfo(frameWidth, frameHeight, razao, _visible = true) {
    var offset = -20;
    var arrowOffset = offset / 2;
    var arrowSize = 10;
    var stroke = 0.0;
    var lineWidth = 0.0;

    //console.log('frameWidth,frameHeight :'+frameWidth,frameHeight)

    var group = new Konva.Group({ visible: _visible, name: 'groupLabels' });
    var leftLabel = new Konva.Label({ rotation: 90 });

    const proporcao = (config.escalaY + Number(config.escala))

    var leftText = new Konva.Text({
        fontSize: 12,
        text: Math.round((frameHeight) * config.escalaY) + 'mm',
        padding: 2,
        fill: 'black',
        visible: _visible,
        name: 'leftText'
    });

    leftLabel.add(leftText);
    leftLabel.position({
        x: leftText.x() + 20,
        y: frameHeight / 2 - leftText.height() / 2
    });


    var bottomLabel = new Konva.Label({});


    var bottomText = new Konva.Text({
        fontSize: 12,
        text: Math.round(((frameWidth) * config.escalaX)) + 'mm',
        padding: 2,
        fill: 'black',
        name: 'bottomText'

    });

    bottomLabel.add(bottomText);
    bottomLabel.position({
        x: frameWidth / 2 - bottomText.width() / 2,
        y: frameHeight - 25
    });

    group.add(leftLabel, bottomLabel);

    return group;
}

function drawLayer(nome_parede, lenX, lenY) {
    $('#style-content-layer').remove();

    let view = $('.container-fluid');
    let lenX_view = window.innerWidth * 0.8;
    let lenY_view = window.innerHeight;

    let razaoY = lenY_view / lenY;
    let razaoX = lenX_view / lenX;

    let razao = undefined;
    let razaoMaior = 1;
    let razaoMenor = 1;


    if (razaoX < razaoY) {
        razaoMaior = razaoY;
        razaoMenor = razaoX;

    } else {
        razaoMaior = razaoX;
        razaoMenor = razaoY;
    }

    if (lenX >= lenY && lenX <= lenX_view) {
        razao = (razaoMaior).toFixed(5);
        config.escala = Number((razaoMaior).toFixed(5));

        console.log('UTIL')
    } else {
        razao = (razaoMenor).toFixed(5);
        config.escala = Number((razaoMenor).toFixed(5));
    }

    lenX = lenX * razao;
    lenY = lenY * razao;
 
    let marginX = Number(Math.abs((lenX - lenX_view)) / 2);
    let marginY = Number(Math.abs((lenY - lenY_view)) / 2);


    let width  = Number(lenX);
    let height = Number(lenY);

    $('.menu-opcoes').append(`
        <div class="buttons">
        </div>
        `);

    $('.layers').append(`
        <div class="content-layer ${nome_parede}">
        <div id=${nome_parede} class="paredes_container" style="width: ${Number.parseInt(Math.round(width))}px!important;height: ${Number.parseInt(Math.round(height))}px!important">

        </div>
        </div>

        <style id="style-content-layer">

        .paredes_container{
            margin-left: ${Number.parseInt(marginX)}px!important;    
        }
        </style>
        `);

    return { width: width, height: height };
}

function createTransformer(contadorLayer, nome_parede) {
    LAYER[`${contadorLayer}`]['tr'] = new Konva.Transformer({
        relativeToDiv: nome_parede,
        isTransformer: true,
        rotateEnabled: false,
        boundBoxFunc: function (oldBoundBox, newBoundBox) {

            LAYER[1].tr.setZIndex(1);
            LAYER[1].layer.draw()
            const node = LAYER[`${contadorLayer}`]['tr'].getNode();
            let imagemProduto = node.attrs.img;
            let componente = node

            const ___Opcoes = componente.findOne('.fillShape').attrs._OPCOES;


            MAX_WIDTH = $('.konvajs-content').width().toFixed();
            MAX_HEIGHT = $('.konvajs-content').height().toFixed(2);

            if (newBoundBox.x < 0) {
                newBoundBox.x = 0
                newBoundBox.width = oldBoundBox.width;
            }

            if (newBoundBox.y < 0) {
                newBoundBox.y = 0
                newBoundBox.height = oldBoundBox.height;
            }

            if (newBoundBox.x + newBoundBox.width >= MAX_WIDTH || newBoundBox.y + newBoundBox.height >= MAX_HEIGHT) {
                newBoundBox = oldBoundBox;
            }

            LAYER[`${contadorLayer}`]['layer'].children.each(function (child) {
                if (child === node) {
                    return;
                }

                if (child.getAttr('isTransformer')) {
                    return;
                }
                if (haveIntersection(node.getClientRect(), child.getClientRect())) {
                    newBoundBox = oldBoundBox;
                }
            });

            if (newBoundBox.x <= 3) {
                newBoundBox.x = 0;
            }

            if (newBoundBox.y <= 3) {
                newBoundBox.y = 0;
            }


            if (newBoundBox.width >= (MAX_WIDTH - 4)) {
                newBoundBox.width = Number(MAX_WIDTH) + 0.4
            }

            if (newBoundBox.height >= (MAX_HEIGHT - 4 )) {
                newBoundBox.height = Number(MAX_HEIGHT)
            }

            let width_create = newBoundBox.width;
            let height_create = newBoundBox.height;
            let relativeToDiv_create = LAYER[`${contadorLayer}`]['tr'].attrs.relativeToDiv;
            let contadorLayer_create = 1;//contadorLayer;
            let imagemProduto_create = imagemProduto;
            let id_produto_create = componente.attrs.idProduto;
            let opcoes_prod_create = ___Opcoes;

            let newComponent = createShape(width_create, height_create, relativeToDiv_create, contadorLayer_create, imagemProduto_create, id_produto_create, opcoes_prod_create);

            newComponent.attrs.idProduto = componente.attrs.idProduto
            LAYER[`${contadorLayer}`].key = newComponent

            LAYER[`${contadorLayer}`]['tr'].setNode(newComponent);
            LAYER[`${contadorLayer}`]['layer'].children[componente.index].destroy();
            LAYER[`${contadorLayer}`]['layer'].add(newComponent);

            LAYER[1].stage.batchDraw();

            return newBoundBox;
        }
    });

    LAYER[`${contadorLayer}`]['layer'].add(LAYER[`${contadorLayer}`]['tr']);
    LAYER[`${contadorLayer}`]['layer'].draw();
}

function instaceStageAndLayer(dimensoes, contadorLayer, nome_parede) {
    LAYER[`${contadorLayer}`] = [];
    LAYER[`${contadorLayer}`]['key'] = undefined;
    LAYER[`${contadorLayer}`]['ESCALA'] = 1;
    LAYER[`${contadorLayer}`]['DELTA'] = 1;
    LAYER[`${contadorLayer}`]['posValid'] = 0;
    LAYER[`${contadorLayer}`]['attachTranformer'] = undefined;
    LAYER[`${contadorLayer}`]['verifyIntersection'] = false;
    LAYER[`${contadorLayer}`]['stage'] = new Konva.Stage({
        container: nome_parede,
        width:  dimensoes.width,
        height: dimensoes.height
    });

    LAYER[`${contadorLayer}`]['layer'] = new Konva.Layer();
    LAYER[`${contadorLayer}`]['stage'].add(LAYER[`${contadorLayer}`]['layer']);
}

function AddEventKeydown(contadorLayer) {

    LAYER[`${contadorLayer}`]['stage'].container().tabIndex = -1;
    LAYER[`${contadorLayer}`]['stage'].container().addEventListener('keydown', function (e) {


        if (e.keyCode === 37) {
            //////console.log('37')
            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({ x: LAYER[`${contadorLayer}`]['key'].x() - LAYER[`${contadorLayer}`]['DELTA'], y: LAYER[`${contadorLayer}`]['key'].y() }).length < 3) {
                LAYER[`${contadorLayer}`]['key'].x(LAYER[`${contadorLayer}`]['key'].x() - LAYER[`${contadorLayer}`]['DELTA']);
            }

        } else if (e.keyCode === 38) {
            //////console.log('38')

            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({ y: LAYER[`${contadorLayer}`]['key'].y() - LAYER[`${contadorLayer}`]['DELTA'], x: LAYER[`${contadorLayer}`]['key'].x() }).length < 3) {
                LAYER[`${contadorLayer}`]['key'].y(LAYER[`${contadorLayer}`]['key'].y() - LAYER[`${contadorLayer}`]['DELTA']);
            }


        } else if (e.keyCode === 39) {
            //////console.log('39')

            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({ x: LAYER[`${contadorLayer}`]['key'].x() + LAYER[`${contadorLayer}`]['key'].width() + LAYER[`${contadorLayer}`]['DELTA'], y: LAYER[`${contadorLayer}`]['key'].y() }).length <= 3) {
                LAYER[`${contadorLayer}`]['key'].x(LAYER[`${contadorLayer}`]['key'].x() + LAYER[`${contadorLayer}`]['DELTA']);
            }
        } else if (e.keyCode === 40) {
            //////console.log('40')

            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({ y: LAYER[`${contadorLayer}`]['key'].y() + LAYER[`${contadorLayer}`]['key'].height() + LAYER[`${contadorLayer}`]['DELTA'], x: LAYER[`${contadorLayer}`]['key'].x() }).length <= 3) {
                LAYER[`${contadorLayer}`]['key'].y(LAYER[`${contadorLayer}`]['key'].y() + LAYER[`${contadorLayer}`]['DELTA']);
            }
        } else {
            return;
        }

        e.preventDefault();
        LAYER[`${contadorLayer}`]['layer'].batchDraw();
    });

}

function AddEventKeyUp(contadorLayer){
    LAYER[`${contadorLayer}`]['stage'].container().addEventListener('keyup', function (e) {
        LAYER[`${contadorLayer}`]['layer'].children.each(function (group) {
    
            if (group === LAYER[1].key) {
                return;
            }
    
            if (group.attrs.isTransformer) {
                return;
            }
                
            console.log(group.getClientRect());
            console.log(LAYER[1].key.getClientRect());
            if (haveIntersection(group.getClientRect(), LAYER[1].key.getClientRect())) {
    
                console.log(LAYER[1].key.posX);
                LAYER[1].key.setX(LAYER[1].key.posX);
                LAYER[1].key.setY(LAYER[1].key.posY);
    
                LAYER[`${contadorLayer}`]['layer'].draw();
                Intersection = true;
                LAYER[`${contadorLayer}`]['verifyIntersection'] = true;
    
            } else {
                //if (!LAYER[`${contadorLayer}`]['verifyIntersection']) {
                LAYER[1].key.posX = LAYER[1].key.getX();
                LAYER[1].key.posY = LAYER[1].key.getY();
                LAYER[`${contadorLayer}`]['verifyIntersection'] = false;
                //}
            }
        });
        LAYER[`${contadorLayer}`]['posValid'] = undefined;
    });
}

function AddEventClick(contadorLayer) {

    LAYER[`${contadorLayer}`]['layer'].on('click tap', function (e) {
        e.target.parent.posX = e.target.parent.getX();
        e.target.parent.posY = e.target.parent.getY();
        teste = e.target.parent
        
        deleteClassButtons();
        
        $('.buttons .btn-remove').remove();
        $(`.buttons`).append(`<a href="#"  class="btn-remove" onclick="deleteElement(${e.target.parent.index},${contadorLayer})" class="mb-4"><img  class="my-4" src="img/remove.png" style="width: 50px;" /></a>`);
        
        
        LAYER[`${contadorLayer}`]['tr'].detach(LAYER[`${contadorLayer}`]['attachTranformer']);
        LAYER[`${contadorLayer}`]['attachTranformer'] = e.target.parent;
        LAYER[`${contadorLayer}`]['key'] = LAYER[`${contadorLayer}`]['attachTranformer'];
        LAYER[`${contadorLayer}`]['tr'].attachTo(LAYER[`${contadorLayer}`]['attachTranformer']);
        LAYER[`${contadorLayer}`]['layer'].draw();
    });

}

function deleteClassButtons(){
    if( $('.buttons').length > 1){
        for (let index = 1; index < $('.buttons').length; index++) {
            $('.buttons')[index].remove();
        }
    }
}

function dragStartEvent(contadorLayer) {
    LAYER[`${contadorLayer}`]['layer'].on('dragstart touchstart', function (e) {
        LAYER[`${contadorLayer}`]['tr'].detach(LAYER[`${contadorLayer}`]['attachTranformer']);
        LAYER[`${contadorLayer}`]['verifyIntersection'] = false;
        LAYER[`${contadorLayer}`]['posValid'] = { x: e.target.attrs.x, y: e.target.attrs.y };
    });
}

function dragEndEvent(contadorLayer) {
    LAYER[`${contadorLayer}`]['layer'].on('dragend touchend', function (e) {
        var target = e.target;
        var targetRect = e.target.getClientRect();

        LAYER[`${contadorLayer}`]['layer'].children.each(function (group) {

            if (group === target) {
                return;
            }

            if (group.attrs.isTransformer) {
                return;
            }

            if (haveIntersection(group.getClientRect(), targetRect)) {
                target.setX(LAYER[`${contadorLayer}`]['posValid'].x);
                target.setY(LAYER[`${contadorLayer}`]['posValid'].y);
                LAYER[`${contadorLayer}`]['layer'].draw();
                Intersection = true;
                LAYER[`${contadorLayer}`]['verifyIntersection'] = true;

            } else {
                if (!LAYER[`${contadorLayer}`]['verifyIntersection']) {
                    target.posX = target.getX();
                    target.posY = target.getY();
                    LAYER[`${contadorLayer}`]['verifyIntersection'] = false;
                }
            }
        });
        LAYER[`${contadorLayer}`]['posValid'] = undefined;
    });
}







/* Async functions*/

/*CreateLayer é um controlodor de todos os processos de criação de uma Layer */
async function createLayer(nome_parede, lenX, lenY) {
    
    let dimensoes       = drawLayer(nome_parede, lenX, lenY);
    const largura_layer = lenX;
    const altura_layer  = lenY;
    const contadorLayer = contador;
    /*const contadorLayer = contador.Increment();*/

    instaceStageAndLayer(dimensoes, contadorLayer, nome_parede);

    createTransformer(contadorLayer, nome_parede);

    AddEventKeydown(contadorLayer);

    AddEventKeyUp(contadorLayer);

    AddEventClick(contadorLayer);

    dragStartEvent(contadorLayer);

    dragEndEvent(contadorLayer)

    /* Set valores de escala de acordo com o tamanho real da parede e tamanho virtual da parede para os eixos X e Y  */
    config.escalaX = Number((largura_layer / $('.konvajs-content').width()).toFixed(5));
    config.escalaY = Number((altura_layer  / $('.konvajs-content').height()).toFixed(5));

    // if(config.escalaX > config.escalaY){
    //     config.escalaY = config.escalaX
    // }else{
    //     config.escalaX = config.escalaY
    // }

    return await LAYER[`${contadorLayer}`]['layer'];


}

async function getOpitonsComponent() {

    if (LAYER[1].key === undefined) {
        alert('Selecione o Componente')
        return;
    }

    let arrayOpcoes = [];
    let formOpitons = [];
    $('.opcoes').empty()

    idComponente = LAYER[1].key.attrs.idProduto;

    const _OPCOES = await api.AJAXOpcaoComponente(idComponente);

    $.each(_OPCOES.objects.opcoes, (chave, opcoes) => {

        arrayOpcoes.push({ "atributo": opcoes.nome, "valor": opcoes.valor })
        formOpitons.push(opcoes.nome);

        if (opcoes.formato_opcao === "LISTA") {
            HTML.lista(opcoes);
        }

        if (opcoes.formato_opcao === "MANUAL") {
            HTML.manual(opcoes);
        }
    });

    $('.opcoes').append(`
        <button type="button" onclick="saveOptions()" class="btn btn-primary btn-lg btn-block">Salvar</button>
        `);

    __FORMOPC = formOpitons;
    LAYER[1].key.findOne('.fillShape').attrs._OPCOES = [];
}



