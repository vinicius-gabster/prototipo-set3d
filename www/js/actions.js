const actions = {
	listLayers: async (idCliente)=>{

		$('.opcoes').empty();
		$('.opcoes').append(`<div class="listLayers" style="display:grid"></div>`);

		let response = await api.AJAXComponentsInProject();


		$.each(response, (key, layer)=>{
			$('.listLayers').append(`<button type="button" class="btn border border-primary bg-light btn-link my-1 mx-1" onclick="actions.loadLayer(${layer.id})">${layer.nome}</button>`);
		});
	},

	loadLayer: async  id =>{
		
		deleteClassButtons();

		let deleteChildrens = await actions.deleteChildrens();
		$('.principal').remove();
		$('.btn-remove').remove();


		let response = await api.getLayer(1,1,id)

		let rendered = konva.renderLayer(response, false);

	},

	deleteChildrens: async ()=>{
		try{
			//LAYER[1].tr.detach(LAYER[1].key);
			if (LAYER[1].layer.children.length > 1) {
				while(LAYER[1].layer.children.length > 1){
					LAYER[1].layer.children[1].destroy();
					LAYER[1].layer.draw();

				}
			}

			LAYER[1].layer.draw();


			return await true;


		}catch(erro){
		
			return await true;
			
		}

	},

	alterLayer: (obj) => {
		const wid = $('#width-label').val();
		const hei = $('#height-label').val();

		const json = {
			width: wid,
			height: hei,
			cd_layer: sessionStorage.getItem('id_Layer')
		}


		let response = api.setDimensoesLayer(json);

		response.then(data => {
			console.log(data)
			if (data.status) {

				actions.loadLayer(Number.parseInt(sessionStorage.getItem('id_Layer')));
			}
		})
	}
}

let config = {
	escala: 1,
	escalaX: 1,
	escalaY: 1,
}
