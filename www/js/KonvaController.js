class KonvaController{

	constructor(){
		this.bibliotecas = [];	 
	}

	async Getbibliotecas(){
		return await $.ajax({
			type: "GET",
			url: URL_API + "/ws/b/c/?format=json",
			contentType: 'json',
			headers: {
				"Authorization": "ApiKey "+USER+":"+APIKEY+"",
				"Content-Type" : 'application/json',
			},
			success: async function (data){
				await console.log(data.status.bibliotecas);
			},
			error: function(data){
				alert("error")
			}
		})
		.done(function(data){
			return data;	
		})

	}

	async getOpcoesComponente(idProduto){
		let response = [];
		return await $.ajax({
			type: "GET",
			url: URL_API + "/api/v1/via_opcao_componente/?componente="+idProduto+"&format=json",
			contentType: 'json',
			headers: {
				"Authorization": "ApiKey "+USER+":"+APIKEY+"",
				"Content-Type" : 'application/json',
			},
			success: async function (data){
				response = data;
				await console.log(data)

			}
		})
		.done(async function(data){
			console.log('done')
			return data;	
		})

		
	}

	async MenuLeft(){
		let arr = []
		const bibliotecas_fabrica = await api.AJAXBibliotecas()
		//console.log('bibliotecas_fabrica');
		console.log(bibliotecas_fabrica);
		$.each(bibliotecas_fabrica.status.bibliotecas, (k, grupos)=>{

			$.each(grupos.grupos, function(k1, subgrupos){
				//subgrupos 0:aereos 1: balcoes
				//console.log(subgrupos)

				$('.opcoes').append(`

					<div>
						<a class="btn btn-light d-block" type="button" data-toggle="collapse" data-target="#multiCollapseExample3${subgrupos.id}" aria-expanded="false" aria-controls="multiCollapseExample3${subgrupos.id}">${subgrupos.nome}</a>
						<div class="col">
							<div class="collapse multi-collapse" id="multiCollapseExample3${subgrupos.id}">
								<div id="${subgrupos.nome}" class="card card-body d-block ${subgrupos.nome}">
								</div>
							</div>
						</div>
					</div>

					`);

				$.each(subgrupos.subgrupos, (k2,colecoes)=>{
					//console.log(k3,colecoes_subgrupo);
					$.each(colecoes.itens, (k4,itens)=>{
						$(`#${subgrupos.nome}`).append(`
							<a onclick="AddComponente(1, principal,'${itens.url_preview}',${itens.id})"><img src="${itens.url_preview}" class="rounded img-thumbnail img-produto " alt="${itens.nome}"></a>
						`)
					});
				});

			});

		});

	}

	MenuLeft1() {

		$('.opcoes').append(`
			<div>
				<a class="btn btn-light d-block" type="button" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3">Fixos</a>
				<div class="col">
					<div class="collapse multi-collapse" id="multiCollapseExample3">
						<div id="" class="card card-body d-block">
						<button onclick="AddComponente(1,'principal','img/porta.png',null)" class="btn btn-primary btn-block">
						porta
						</button>				
						<button onclick="AddComponente(1,'principal','img/janela.png', null)" class="btn btn-primary btn-block">
						Janela
						</button>	
						</div>
					</div>
				</div>
			</div>
		`);

	}

}

let controller = new KonvaController();
