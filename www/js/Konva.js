const konva = {
	renderLayer: async (response,rendering=true)=>{

        let promise = new Promise(function(resolve, reject) {  
            const widthWindow = $('.konvajs-content').width();
            const heightWindow = $('.konvajs-content').height();

            let razao = konva.isMaior(response.width_last_layer , response.height_last_layer);
            let ScaleLayer = 1

            //if (rendering) {
                const escalaX =  widthWindow / response.width_last_layer 
                const escalaY =  heightWindow / response.height_last_layer 


                if (response.width_last_layer != widthWindow || response.height_last_layer != heightWindow && rendering) {

                    ScaleLayer = escalaX//((1 - razao) + 1);
                }
           // }



            $.each(response.json,   function(key, KONVA){
                let arrayOptions = [];
                $.each(KONVA.OPCOES, (key,value)=>{
                    arrayOptions[key] = value;
                });
                

                let x = (KONVA.componente.attrs.x >= 0 ? KONVA.componente.attrs.x : 0);
                let y = (KONVA.componente.attrs.y >= 0 ? KONVA.componente.attrs.y : 0);

                console.log(KONVA.componente.attrs.x * razao);
                console.log(Number.parseFloat(razao));



                var group = new Konva.Group({
                    relativeToDiv: KONVA.componente.attrs.relativeToDiv,
                    name:KONVA.componente.attrs.name,
                    x:   Number.parseFloat(x * escalaX) ,
                    y:   Number.parseFloat(y * escalaY) ,
                    img: KONVA.componente.attrs.img,
                    idProduto: KONVA.componente.attrs.idProduto,
                    draggable: KONVA.componente.attrs.draggable,
                    _OPCOES:   arrayOptions,
                    dragBoundFunc: function(pos) {

                        try{
                            MAX_WIDTH  = document.getElementsByClassName(`konvajs-content`)[0].clientWidth;
                            MAX_HEIGHT = document.getElementsByClassName(`konvajs-content`)[0].clientHeight;
                            console.log('max: '+ MAX_HEIGHT,MAX_WIDTH)
                        }catch(erro){
                            let DIV    = $(`.${this.attrs.relativeToDiv}`)
                            MAX_WIDTH  = DIV.width();
                            MAX_HEIGHT = DIV.height();
                        }

                        let x_abs = pos.x
                        let y_abs = pos.y;

                        if (pos.x < 0 ) {
                            x_abs = 0;
                        }

                        if (pos.y < 0 ) {
                            y_abs = 0;
                        }

                        let findOne = this.findOne('.fillShape');

                        if ( pos.y + (this.findOne('.fillShape').getClientRect().height) >= MAX_HEIGHT ) {
                            y_abs = MAX_HEIGHT - this.findOne('.fillShape').getClientRect().height;
                        }

                        if ((this.findOne('.fillShape').getClientRect().width)+ pos.x >= MAX_WIDTH ) {
                            x_abs = MAX_WIDTH - (this.findOne('.fillShape').getClientRect().width);
                        }

                        return {
                            x: x_abs,
                            y: y_abs
                        }
                    }

                })

                console.log(KONVA.componente.children[0].attrs)
                var shape = new Konva.Image({
                    height: KONVA.componente.children[0].attrs.height *  escalaY,
                    width:  KONVA.componente.children[0].attrs.width  *  escalaX,
                    scale:  ScaleLayer,
                    fill:  '#00000040',
                    name:  'fillShape',
                });

                var imageObj1    = new Image();
                
                imageObj1.onload = ()=>{
                    shape.image(imageObj1);
                };

                imageObj1.src = KONVA.componente.attrs.img;



                var label = createInfo(shape.attrs.width,shape.attrs.height,ScaleLayer);
                group.add(shape);
                group.add(label);

                var boundingBox = shape.getClientRect({ relativeTo: group });

                var box = new Konva.Rect({
                    x: boundingBox.x,
                    y: boundingBox.y,
                    width: boundingBox.width,
                    height: boundingBox.height,
                });
                //group.add(box);
                group.posX = group.x(); 
                group.posY = group.y();

                LAYER[1].layer.add(group);
                LAYER[1].layer.draw();
                return group;
            });

            resolve('Promise is resolved!');

        });

    promise.then(function(data){
        console.log(data)
        setTimeout(()=>{
            LAYER[1].stage.batchDraw();
            LAYER[1].stage.batchDraw();
            LAYER[1].layer.batchDraw();
            
        }, 1)
    });

    },

    drawLayer: (data, id_Layer) => {
        
        const width  = Number.parseInt(data.width);
        const height = Number.parseInt(data.height);

        createLayer('principal', width,height);
        
        $('#width-label').val(Number.parseInt(data.width))
        $('#height-label').val(Number.parseInt(data.height))
        
        sessionStorage.setItem('id_Layer',id_Layer);

        let layerInWindow =  $('.content-layer , .principal');

        if (layerInWindow.length > 1) {
            layerInWindow[1].remove();
        }

    },

    isMaior: (r1,r2) => {
        if (r1 > r2) {
            return Number.parseFloat((r1 / r2));
        }else{
            return Number.parseFloat((r2 / r1));
        }
    },
}