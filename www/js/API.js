const api = {

    URL_API: 'http://127.0.0.1:8000',
    APIKEY : 'e76d1238facc70019bea81037a92eca701002611',
    USER   :  'vinicius.silva@gabster.com.br',

    setDimensoesLayer: async (json) => {
        return await $.ajax({
            type: "POST",
            url: "/api/index.php/layers/uptDimensoesLayer",
            headers:{
                "X-USERNAME" : api.USER,
                "X-API-KEY" : api.APIKEY,
            },
            contentType: 'application/json',
            data: JSON.stringify(json),
            success: function (data){ 
            }
        })
    },

    saveProject: async function(idLayer=1) {
        let childrens = LAYER[idLayer]['layer'].children;
        let data = [];

        childrens.each(group =>{
            if (group.attrs.isTransformer) {
                return;
            }

            let children = group;
            children = JSON.stringify(children);
            children = JSON.parse(children);
            children = JSON.parse(children);

            children.attrs._OPCOES = group.attrs._OPCOES
            let json = { 
                "componente" : children,
                "OPCOES" : { ...group.attrs._OPCOES }
            }

            data.push(json)

        });
        
        let post = {

            "cd_projeto"    :  Number.parseInt(sessionStorage.getItem('cd_projeto')),
            "cd_layer"      :  Number.parseInt(sessionStorage.getItem('id_Layer')),
            "data"          :  data,
            "width"         : $('.paredes_container').width()  + 2,
            "height"        : $('.paredes_container').height() + 2,
        }

        //data.cd_projeto = Number.parseInt(sessionStorage.getItem('cd_projeto')); 

        let req = await $.ajax({
            type: "POST",
            url: "/api/index.php/layerSave/",
            headers:{
                "X-USERNAME" : api.USER,
                "X-API-KEY" : api.APIKEY,
            },
            contentType: 'application/json',
            data: JSON.stringify(post),
            success: async function (data){
                alert('Layer Salva com Sucesso!!!')
            },
            error: function(data){
                alert("error")
            }
        })

    },

    AJAXComponentsInProject:async function() {

        const data = {
            "id_projeto" : Number.parseInt(sessionStorage.getItem('cd_projeto')),
            "id_cliente" : Number.parseInt(sessionStorage.getItem('cd_cliente'))
        }

        return await $.ajax({
            type: "POST",
            url: "/api/index.php/layers/",
            headers:{
                "X-USERNAME" : api.USER,
                "X-API-KEY" : api.APIKEY,
            },
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data){ 
                console.log(data)
            }
        })

    },

    AJAXBibliotecas: async function() {
        let req = await $.ajax({
            type: "GET",
            url: api.URL_API + "/ws/b/c/?format=json",
            contentType: 'json',
            headers: {
                "Authorization": "ApiKey "+api.USER+":"+api.APIKEY+"",
                "Content-Type" : 'application/json',
            },
            success: async function (data){
                await console.log(data.status.bibliotecas);
            },
            error: function(data){
                alert("error")
            }
        });

        return await req;
    },

    AJAXOpcaoComponente:async function (idProduto){
        let request = await $.ajax({
            type: "GET",
            url: api.URL_API + "/api/v1/via_opcao_componente/?componente="+idProduto+"&format=json",
            contentType: 'json',
            headers: {
                "Authorization": "ApiKey "+api.USER+":"+api.APIKEY+"",
                "Content-Type" : 'application/json',
            },
            success: async function (data){
                await console.log(data)

            }
        });

        return request;
    },

    acabamentos: async ()=>{
        $.ajax({
            type: "GET",
            url: api.URL_API + "/ws/b/a/?format=json",
            contentType: 'json',
            headers: {
                "Authorization": "ApiKey "+api.USER+":"+api.APIKEY+"",
                "Content-Type" : 'application/json',
            },
            success: function(data){
                console.log(data)
            }
        })
    },

    projetoCliente: async (email)=>{

        let data = {
            email: email
        }
        
        return await $.ajax({
            type: "POST",
            url: "/api/index.php/GetProjeto/",
            headers:{
                "X-USERNAME" : api.USER,
                "X-API-KEY" : api.APIKEY,
            },
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data){ 
                console.log(data)
                sessionStorage.setItem('cd_cliente', data.id_cliente);
                //alert(Array.isArray(data.id_cliente));

            }
        })
    },

    getLayer:async function(id=1,id_cliente=1,id_Layer) {

        const data = {
            "id_projeto" : Number.parseInt(sessionStorage.getItem('cd_projeto')),
            "id_cliente" : Number.parseInt(sessionStorage.getItem('cd_cliente')),
            "id_layer"   : Number.parseInt(id_Layer)
        }



        return await $.ajax({
            type: "POST",
            url: "/api/index.php/LayerGetId/",
            headers:{
                "X-USERNAME" : api.USER,
                "X-API-KEY" : api.APIKEY,
            },
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data){ 
                konva.drawLayer(data , id_Layer);
            }
        })

    },

    novoProjeto: async ()=>{
        const nome_projeto = $('#nome_novo_projeto').val();
        const num_layers = $('#num_layer_new_project').val();

        const data = {
          "id_cliente"      : Number.parseInt(sessionStorage.getItem('cd_cliente')),
          "nome_projeto"    : nome_projeto,
          "num_layers"      : Number.parseInt(num_layers)
        };

        console.log(data)


        return await $.ajax({
            type: "POST",
            url: "/api/index.php/NovoProjeto/",
            headers:{
                "X-USERNAME" : api.USER,
                "X-API-KEY" : api.APIKEY,
            },
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data){ 
                console.log(data)
                sessionStorage.setItem('cd_projeto', Number.parseInt(data.codigo))
                window.location.replace('index.html')
            }
        })
    },
}

