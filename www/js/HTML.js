const HTML = {
	manual: opcao => {
		$('.opcoes').append(`
			<div " class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text" id="basic-addon1">${opcao.label}</span>
				</div>
				<input id="${opcao.nome}" type="text" class="form-control" placeholder="${opcao.label}" aria-label="${opcao.label}" aria-describedby="basic-addon1">
			</div>
		`);
	},

	lista: opcao => {
		$('.opcoes').append(`
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text" for="inputGroupSelect01">${opcao.label}</label>
				</div>
				<select id="${opcao.nome}" class="custom-select" id="inputGroupSelect01">
					${HTML.opcoes(opcao.opcao_componente_lista)}
				</select>
			</div>
		`);
	},

	opcoes: opcoes => {
		let stringHTML = ``;

		$.each(opcoes, (k,opcao)=>{
			if (opcao.valor_padrao) {
				stringHTML = stringHTML + `<option selected value="${opcao.valor}">${opcao.texto}</option>`
			}else{
				stringHTML = stringHTML + `<option value="${opcao.valor}">${opcao.texto}</option>`
			}
		});

		return stringHTML;
	},

	validaForm: ()=>{
		let arrayReturn = [];

		$.each(__FORMOPC, (key,value)=>{
			let val = $('#'+value).val();
			let json  = []
			json[value] = val

			arrayReturn.push(json);
		})

		return arrayReturn;
	}
}